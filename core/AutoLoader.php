<?php
/**
 * Created by IntelliJ IDEA.
 * User: Виталий
 */
class AutoLoader {
    public static function autoload ($class) {
        $class = str_replace('\\', '/', $class);
        require_once ($class.'.php');
    }
}
spl_autoload_register(['AutoLoader', 'autoload']);