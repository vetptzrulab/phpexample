<?php
/**
 * Created by IntelliJ IDEA.
 * User: Виталий
 */

namespace core\helpers;

class WebSpider {

    public static function getHTML ($url, $timeout = 15) {
        $time_start = microtime(true);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $Result["Html"] = curl_exec($ch);
        $Result["Info"] = curl_getInfo($ch);
        $time_end = microtime(true);
        $Result["TimeSet"] = $timeout;
        $Result["TimeTotal"] = $time_end - $time_start;
        return $Result;
    }

}