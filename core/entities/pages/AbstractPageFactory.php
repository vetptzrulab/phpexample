<?php
/**
 * Created by IntelliJ IDEA.
 * User: Виталий
 */

namespace core\entities\pages;


abstract class AbstractPageFactory {

    protected function createObject ($type) {
        switch ($type) {
            case 'youtube':
                $object = new Youtube();
                break;
            case 'rutube':
                $object = new Rutube();
                break;
            default:
                return false;
        }
        return $object;
    }
}