<?php
/**
 * Created by IntelliJ IDEA.
 * User: Виталий
 */

namespace core\entities\pages;



class Youtube implements IPage  {

    private $html = false;

    use TraitPage;

    public function findTitle($url) {
        $this->getPage($url);
        if ($this->html && preg_match("/<span class=\"stat view-count\">([^<]{1,})<\/span>/iU", $this->html, $out)) {
            return trim(preg_replace("/[^0-9]/", "", $out[1]));
        } else {
            return false;
        }
    }
}