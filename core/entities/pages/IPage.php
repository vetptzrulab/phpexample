<?php
/**
 * Created by IntelliJ IDEA.
 * User: Виталий
 */

namespace core\entities\pages;

interface IPage {
    public function findTitle ($url);
}