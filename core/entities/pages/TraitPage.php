<?php
/**
 * Created by IntelliJ IDEA.
 * User: Виталий
 */

namespace core\entities\pages;

use core\helpers\WebSpider;

trait TraitPage {
    private function getPage ($url, $timeout = 15) {
        $this->html = false;
        $result = WebSpider::getHTML($url, $timeout);
        if ($result["Info"]["http_code"] == 200) {
            $this->html = $result["Html"];
            return true;
        } else {
            return false;
        }
    }
}