<?php
/**
 * Created by IntelliJ IDEA.
 * User: Виталий
 */
namespace core\entities\pages;

class PageFactory extends AbstractPageFactory {

    public function getSpider ($type) {
        return $this->createObject($type);
    }

}