<?php
/**
 * Created by IntelliJ IDEA.
 * User: Виталий
 */

namespace core\entities\pages;


class Rutube implements IPage {

    private $html = false;

    use TraitPage;

    public function findTitle($url) {
        $this->getPage($url);
        if ($this->html && preg_match("/content=\"UserViews:([0-9]{1,})\"/iU", $this->html, $out)) {
            return trim($out[1]);
        } else {
            return false;
        }
    }
}