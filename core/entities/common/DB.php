<?php
/**
 * Created by IntelliJ IDEA.
 * User: Виталий
 */

namespace core\entities\common;

class DB {

    private $_mysqli;
    private $_query;
    private $_result;
    private $_count = 0;

    static protected $_instance;

    /**
     * Repository constructor.
     */
    private function __construct() {
        $this->_mysqli = new \mysqli(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DATABASE);
        if ($this->_mysqli->connect_errno) {
            die("Не удалось подключиться: ". $this->_mysqli->connect_error);
        }
    }

    /**
     * @return Repository
     */
    public static function getInstance () {
        if (empty(self::$_instance)) {
            self::$_instance = new DB();
        }
        return self::$_instance;
    }

    public function query ($sql) {
        $this->_result = [];
        $this->_count = 0;
        if ($this->_query = $this->_mysqli->query($sql)) {
            $this->_count = $this->_query->num_rows;
            while ($row = $this->_query->fetch_object()) {
                $this->_result[] = $row;
            }
        }
        return $this;
    }

    public function results () {
        return $this->_result;
    }

    public function count () {
        return $this->_count;
    }

    /**
     *
     */
    private function __wakeup() {
        // TODO: Implement __wakeup() method.
    }

    /**
     *
     */
    private function __clone() {
        // TODO: Implement __clone() method.
    }
}