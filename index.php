<?php
/**
 * Created by IntelliJ IDEA.
 * User: Виталий
 */

use core\entities\common\DB;
use core\entities\pages\PageFactory;

require_once ("./core/config/database.php");
require_once ("./core/AutoLoader.php");


function getTargets ($sql) {
    $targets =  DB::getInstance()->query($sql);
    if ($targets->count()) {
        foreach ($targets->results() as $index => $val) {
            yield $val->id => $val;
        }
    }

}

$pageFactory = new PageFactory();

foreach (getTargets("Select * From links") as $id => $val) {
    if ($spyder = $pageFactory->getSpider($val->type)) {
        if ($views = $spyder->findTitle($val->link)) {
            echo "Page {$val->link} on <strong>{$val->type}</strong> have <strong>$views</strong> views <br/>";
        } else {
            echo "Can not get views on <strong>{$val->type}</strong> by URL {$val->link} <br/>";
        }
    } else {
        echo "Error: can not find parser for <strong>{$val->type}</strong><br/>";
    }
}





